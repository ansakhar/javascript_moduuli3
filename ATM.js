import * as readline from "node:readline";


const rl = readline.createInterface({
    input: process.stdin, 
    output: process.stdout, 
    prompt: ">"
});


let saldo = 100;

function saldoKysely() {
    console.log(`saldo on: ${saldo} euroa`);
}

function nosto() {
    rl.question("Syötä tililtä nostettava summa: ( Poistu = 'q'): ", input => {
        if(input === "q") {
            process.exit(1);
        }
        else {
            if (parseInt(input) > saldo) 
                console.log(`tilillä on vain ${saldo} euroa`);
            else
            {
                saldo = saldo - parseInt(input);
                console.log(`${input} nostettu tililtä, saldo on: ${saldo} euroa`);
            }
            main();
        }
    });
    
}

function main() {
    rl.question("Kirjoita pyyntö: 'q'(poistu), 's'(saldokysely), 'n'(nosto), 'h'(hyväntekeväisyys)>: ", pyynto => {
        switch (pyynto) {
            case "q": //quit program
                process.exit(1);
            // falls through
            case "s": //saldokysely
                saldoKysely();
                main();
                break;
            case "n": //nosto
                nosto();
                main();
                break;
            case "h": //hyväntekeväisyys
                //lahjaksi();
                break;
            default:
                console.log(`Received: ${pyynto}. Tuntematon komento`);
                main();
                break;
        }
    });
}


main();