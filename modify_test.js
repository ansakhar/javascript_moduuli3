import * as fs from "fs";
import * as readline from "node:readline";

export const rl = readline.createInterface({
    input: process.stdin, 
    output: process.stdout, 
    prompt: ">"
});


/**
 * Lukee yhteystiedot contacts.json -tiedostosta
 * @returns Array of contact objects
 */
function readContacts() {
    let data; 
    try {
        data = fs.readFileSync("contacts.json", "utf-8");    
    } catch (error) { //no file was found
        data = "[]";
    }
    const contacts = JSON.parse(data); 
    return contacts; 
}


/**
 * Lukee ryhmät groups.json -tiedostosta
 * @returns Array of group objects
 */
function readGroups() {
    let data; 
    try {
        data = fs.readFileSync("groups.json", "utf-8");
    } catch (error) { //no file was found
        data = "[]";
    }
    const groups = JSON.parse(data); 
    return groups; 
}


/**
 * Writes contacts.json file
 * @param {Array} content Array of objects to be saved
 */
function saveContacts(content) {
    fs.writeFileSync("contacts.json", JSON.stringify(content), (err) => {
        if(err) console.log(err); 
        else console.log("wrote output");
    });
}


/**
 * Writes groups.json file
 * @param {Array} content Array of object to be saved
 */
function saveGroups(content) {
    fs.writeFileSync("groups.json", JSON.stringify(content), (err) => {
        if(err) console.log(err); 
        else console.log("wrote output");
    });
}


/**
 * Hakee ryhmistä ryhmän tai luo uuden, jos ryhmää ei löydy. 
 * @param {String} input 
 */
function getGroup(input) {
    const name = input.trim();
    if(name === "") {
        return 0;
    }
    //a. try to find group from existing groups
    const groups = readGroups();
    const groupId = groups.reduce((acc, obj) => {
        if(obj.name === name) { //found correct group
            return acc = obj.id;
        } 
        return acc;
    }, 0);
    //b. group didn't exist, create group
    if(groupId === 0) {
        const maxId = groups.reduce((acc, obj) => {
            if(obj.id > acc) {
                return obj.id;
            } else {
                return acc;
            }
        }, 1);
        const newGroup = {
            name,
            id: (parseInt(maxId)+1)
        };
        groups.push(newGroup); 
        saveGroups(groups);

        return newGroup.id;
    } 
    return groupId; 
}

function modifyGroup() {
    const groups = readGroups();
    console.log("All groups:");
    groups.forEach(element => {
        console.log(element.name);
    });
    
    const kentat = ["phone", "email", "street","notes", "group"];
    const content = readContacts();

    let kentta;
    let groupId;
    let n = 0;
    
    const recursiveAsyncReadLine = function () {
        if (n === 3)
        {
            console.log("Bye!");
            return rl.close(); //closing RL and returning from function.
        }
        else if (n === 0)
        {
            rl.question("Kirjoita ryhmän nimi: ( Poistu = 'q'): ", input => {
                if(input === "q") {
                    process.exit(1);
                }
                groupId = groups.reduce((acc, obj) => {
                    if(obj.name === input) { //found correct group
                        return acc = obj.id;
                    } 
                    return acc;
                }, 0);
                if (!groupId) {
                    console.log(`Ryhmä '${input}' ei löytytnyt.`);
                    recursiveAsyncReadLine();
                }
                else
                {
                    const groupContacts = findGroupContacts(groupId, content);
                    console.table(groupContacts);
                    if (groupContacts.length > 0) 
                    {
                        n++;
                        recursiveAsyncReadLine();
                    }
                    else recursiveAsyncReadLine();
                }
            });
        }
        else if (n === 1) {
            rl.question("Mitä kenttää haluat muuttaa: phone, email, street, notes, group? ", input => {
                kentta = input;
                
                if (kentat.includes(kentta)) 
                {
                    n++;
                    recursiveAsyncReadLine();
                }
                else {
                    console.log(`Kenttä '${kentta}' ei löydy!`);
                    recursiveAsyncReadLine();
                }
            });
        }
        else if (n === 2) {
            rl.question(`uusi ${kentta}: `, input => {
                if (["email", "phone"].includes(kentta))
                    content.forEach(element => {
                        if(element.group === groupId) {
                            const arr = input.split(" ");
                            arr.forEach(arvo => {
                                element[kentta].push(arvo); 
                            });
                            console.log("changed!");
                        }
                    });
                else if ((kentta !== "group")) 
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element[`${kentta}`] = input;
                            console.log("changed!");
                        }
                    });
                else 
                    content.forEach(element => {
                        if(element.group === groupId) {
                            element.group = getGroup(input);
                            console.log("changed!");
                        }
                    });
                saveContacts(content);
                
                n++;
                recursiveAsyncReadLine();
            });
        }
    };
    recursiveAsyncReadLine();

}

function findGroupContacts(groupId, content) {
    const groupContacts = content. filter(contact => contact.group === groupId);
    if(groupContacts.length > 0) {
        return groupContacts;
    }
    else console.log("Ryhmän yhteystietoja ei löydy");
    return false;
}

modifyGroup();