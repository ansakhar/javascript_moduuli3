function print(txt) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt);
            resolve();
        }, 1000);
    });
}

async function countdown() {
    try {
        await print(3);
        await print(2);
        await print(1);
        await print("GO");
    } catch (err) {
        console.log(err);
    }
}

countdown();