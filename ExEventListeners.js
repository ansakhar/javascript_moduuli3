import jokes from "./jokes.js";
document.getElementById ("anyJoke").addEventListener ("click", anyJoke);
document.getElementById ("nerdyJoke").addEventListener ("click", nerdyJoke);
document.getElementById ("allJokes").addEventListener ("click", allJokes);

function anyJoke() {
    const num = getRndInteger(jokes.length);
    const result = `<div><p>any joke: ${jokes[num].joke} </p></div>`;
    document.getElementById("jokes").innerHTML = result;
}

function nerdyJoke() {
    const nerdyJokes = jokes.filter(joke => joke.categories.includes("nerdy"));
    console.log(nerdyJokes);
    const num = getRndInteger(nerdyJokes.length);
    const result = `<div><p>nerdy joke: ${nerdyJokes[num].joke} </p></div>`;
    document.getElementById("jokes").innerHTML = result;
}

function allJokes() {
    const div = document.createElement("div");
    const result = document.createElement("ul");
    jokes.forEach(element => {
        const li = document. createElement("li");
        li. appendChild(document. createTextNode(`${element.joke}`));
        result. appendChild(li);
    });
    div.appendChild(result);
    document.getElementById("jokes").innerHTML = "";
    document.getElementById("jokes").appendChild(div);
}

function getRndInteger(max) {
    return Math.floor(Math.random() * max);
}