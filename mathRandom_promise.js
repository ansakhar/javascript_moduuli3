const getValue = function () {
    return new Promise((res) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

getValue().then(valueOneHere => console.log(`Value 1 is ${valueOneHere.value} and `))
    .then(()=>getValue()).then(valueTwoHere => console.log(`value 2 is ${valueTwoHere.value}`))
    .catch(err => {
        console.log(err);
    });

    