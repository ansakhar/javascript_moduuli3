//https://github.com/cheatsnake/emojihub

axios.get("https://emojihub.herokuapp.com/api/random")
    .then(result => {
        const emojies = result.data.htmlCode;
        const div = document.getElementById("random");
        div.innerHTML = "Random emoji:" + emojies[0] + "<span class = 'span'>" + result.data.name + "</span>";
    })
    .catch(err => {
        console.log(err);
    });

axios.get("https://emojihub.herokuapp.com/api/all")
    .then(result => {
        const allEmojies = result.data;
        const div = document.createElement("div");
        const list = document.createElement("ul");
        allEmojies.forEach(element => {
            const li = document. createElement("li");
            li.innerHTML = element.htmlCode[0] + " " + "<span class = 'span'>" + element.name + "</span>";
            list. appendChild(li);
        });
        div.appendChild(list);
        document.getElementById("all").appendChild(div);
    })
    .catch(err => {
        console.log(err);
    });