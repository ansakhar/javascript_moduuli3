const getValue = function () {
    return new Promise((res) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

//1. promise.then():
let valueOneHere;
getValue()
    .then(val => {
        valueOneHere = val.value;
        //return getValue();
    })
    .then(()=>getValue())
    .then(val => {
        const valueTwoHere = val.value;
        console.log(`Value 1 is ${valueOneHere} and value 2 is ${valueTwoHere}`);
    });

//2. async + await:
async function logRandomValues() {
    try {
        const valueOneHere = await getValue();
        const valueTwoHere = await getValue();
        console.log(`Value 1 is ${valueOneHere.value} and value 2 is ${valueTwoHere.value}`);
    }
    catch (err) {
        console.log(err);
    }
}

logRandomValues(); 

//3. Promise.all:
Promise.all([getValue(), getValue()])
    .then(values => {
        console.log(`Value 1 is ${values[0].value} and value 2 is ${values[1].value}`);
    });
