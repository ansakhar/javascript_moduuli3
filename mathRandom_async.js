const getValue = function () {
    return new Promise((res) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

async function result() {
    try {
        await getValue().then(valueOneHere => console.log(`Value 1 is ${valueOneHere.value} and `));
        await getValue().then((valueTwoHere)=>console.log(`value 2 is ${valueTwoHere.value}`));
    } catch (err) {
        console.log(err);
    }
}

result();
