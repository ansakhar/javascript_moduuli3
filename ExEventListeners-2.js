import jokes from "./jokes.js";

function getRandomFromArray(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

const content = document.getElementById("content");
const nerdyJokes = jokes.filter(joke => joke.categories.includes("nerdy"));
const allJokes = jokes.map(joke => joke.joke).join("</br>");

const functions = [
    function getJoke() {
        const joke = getRandomFromArray(jokes);
        content.innerHTML = joke.joke;
    },
    function getNerdyJoke() {
        const joke = getRandomFromArray(nerdyJokes);
        content.innerHTML = joke.joke;
    },
    function getAllJokes() {
        content.innerHTML = allJokes;
    }
];


const buttonIds = ["getjoke", "getnerdyjoke", "getalljokes"];
const buttons = buttonIds.map(id => document.getElementById(id));
buttons.forEach((button, id) => button.addEventListener("click", functions[id]));