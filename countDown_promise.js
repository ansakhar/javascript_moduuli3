function countDown(message) {
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log(message);
            resolve();
        }, 1000);
       
    });
}

countDown("1")
    .then(() =>countDown("..2"))
    .then(()=>countDown("....3"))
    .then(()=>countDown("GO!"))
    .catch(err => {
        console.log(err);
    });