"use strict";
const colors = ["blue", "red", "yellow", "grey", "green"];
function changeColor() {
    const colorRandom = getRndInteger(colors.length);
    console.log(colorRandom);
    //  const n = document.body;
    const n = document.querySelector("body");
    n.style.backgroundColor = colors[colorRandom];
}
function getRndInteger(max) {
    return Math.floor(Math.random() * max);
}