const axios = require("axios");
const express = require("express");
const cors = require("cors");
const port = 3000;

const app = express();
app.use(cors());

//http://localhost:3000/data
app.get("/data", (req, res) => {
    axios.get("https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations")
        .then(data => {
            res.send(data.data);
        })
        .catch(error => console.error(error));
});

// get-request...async await
app.get("/data", async (request, response, next) => {
    try{
        const data = await axios.get("https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations");
        response.send(data.data);
    } catch (err) {
        console.err(err);
        next(err);
    }

});

//http://localhost:3000/api/v1/civilizations
app.get("*", (req, res) => {
    console.log(req.path);
    axios.get("https://age-of-empires-2-api.herokuapp.com" + req.path)
        .then(data => {
            res.send(data.data);
        })
        .catch(error => console.error(error));
});

app.listen(port);
console.log("listening 3000");
//aja tämä command linellä: npm install express cors 